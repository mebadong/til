# 함수

함수를 이용하면 중복 없이 유사한 동작을 하는 코드를 여러 번 호출할 수 있음

## 함수 선언

`함수 선언(function declaration)` 방식을 이용하면 함수를 만들 수 있음(`함수 선언 방식`은 `함수 선언문`이라고 부르기도 함)

```jsx
function 함수명(매개변수) {
  // ... 함수 본문 ...
}
```

`function` 키워드, 함수 이름, 괄호로 둘러싼 매개변수를 차례로 써주면 함수를 선언할 수 있음

## 지역 변수

함수 내에서 선언한 변수인 `지역 변수(local variable)`는 함수 안에서만 접근할 수 있음

## 외부 변수

함수 내부에서 함수 외부의 변수인 `외부 변수(outer variable)`에 접근, 수정할 수 있음

외부 변수는 지역 변수가 없는 경우에만 사용할 수 있음

함수 내부에 외부 변수와 동일한 이름을 가진 변수가 선언되었다면, **내부 변수는 외부 변수를 가림**

외부 변수는 `전역 변수(global variable)`라고 부름, 변수는 연관되는 함수 내에 선언하고, 전역 변수는 되도록 사용하지 않는 것이 좋음

## 매개변수

`매개변수(parameter)`를 이용하면 임의의 데이터를 함수 안에 전달할 수 있음

매개변수는 `인수(argument)`라고도 불림

### 기본값

매개변수에 값을 전달하지 않으면 그 값은 `undefined`가 됨

매개변수에 값은 전달하지 않아도 그 값이 `undefined`가 되지 않게 하려면 `기본값(default value)`를 설정해 주면 됨

```jsx
function 함수명(parameter1, parameter2 = 'default value') {
  // ... 함수 본문 ...
}
```

### 매개변수 기본값을 설정할 수 있는 또 다른 방법

```jsx
function 함수명(parameter) {
  // 매개변수를 undefined와 비교
  if (parameter === undefined) {
    parameter = 'default value';
  }
}

function 함수명(parameter) {
  // 논리 연산자 ||를 사용
  parameter = parameter || 'default value';
 // 이 경우 parameter로 0, '', false 등의 값이 전달되어도 빈 값으로 판단
}

function 함수명(parameter) {
  // nullish coalescing operator 사용
  parameter = parameter ?? 'default value';
}
```

## 반환 값

함수를 호출했을 때 함수를 호출한 그 곳에 특정 값을 반환하게 할 수 있음, 이 특정 값을 `반환 값(return value)`라고 부름

```jsx
function sum(a, b) {
  return a + b; // a와 b를 더한 값을 반환
}
```

지시자 `return`은 함수 내 어디든 사용할 수 있음

실행 흐름이 지시자 `return`을 만나면 함수 실행은 즉시 중단되고 함수를 호출한 곳에 값을 반환

함수 하나에는 여러 개의 `return` 문이 올 수 있음

`return` 문이 없거나 값이 없이 `return` 지시자만 있는 경우 `undefined`를 반환

## 함수 이름짓기

함수는 어떤 `동작`을 수행하기 위한 코드를 모아놓은 것으로, 함수의 이름은 대개 동사

- `show...`: 무언가를 보여주는 함수
- `get...`: 값을 반환하는 함수
- `calc...`: 무언가를 계산하는 함수
- `create...`: 무언가를 생성하는 함수
- `check...`: 무언가를 확인하고 불린값을 반환하는 함수

함수는 함수 이름에 언급되어있는 동작을 정확히 수행하여야 함

독립적인 두 개의 동작은 독립된 함수 두 개에서 나눠서 수행할 수 있게 해야 함

# 함수 표현식

자바스크립트는 함수를 특별한 종류의 값으로 취급 `일급 객체`

함수는 함수 선언 방식 외에 `함수 표현식(function expression)`을 사용해서 만들 수 있음

```jsx
let 함수명 = function(매개변수) {
  // ... 함수 본문 ...
};
```

함수는 값이기 때문에 `alert`를 이용하여 함수 코드를 출력할 수 있음

변수를 복사해 다른 변수에 할당하는 것처럼 함수를 복사해 다른 변수에 할당할 수 있음

```jsx
function sayHi() {
  alert('Hi!');
}

let func = sayHi;  // 함수 복사

func(); // Hi!
```

## 콜백 함수

```jsx
function ask(question, yes, no) {
  if (confirm(question)) {
    yes();
  } else {
    no();
  }
}

function showOk() {
  alert('동의하셨습니다.');
}

function showCancel() {
  alert('취소 버튼을 누르셨습니다.');
}

ask('동의하십니까?', showOk, showCancel);
```

위의 예제에서 함수 `ask`의 인수, `showOk`와 `showCancel`을 `콜백 함수` 또는 `콜백`이라고 부름

# 화살표 함수

```jsx
let func = (arg1, arg2, ...argN) => 함수 본문
```

위와 같이 코드를 작성하면 `arg1, arg2, ...argN`를 받는 함수 `func`이 만들어짐

함수 `func`은 `=>` 우측의 `표현식(expression)`을 평가하고, 평가 결과를 반환

- 인수가 하나밖에 없는 경우 인수를 감싸는 괄호를 생략할 수 있음
- 인수가 하나도 없을 경우 `()`를 사용하고, 이 때 괄호는 생략할 수 없음

## 본문이 여러 줄인 화살표 함수

```jsx
let func = (arg1, arg2, ...argN) => {
  // ... 함수 본문 ...
};
```

# 브라우저 디버깅

## debugger 명령어

스크립트 내에 `debugger` 명령어를 적어주면 중단점을 설정한 것과 같은 효과

# 객체

`객체형` 타입은 원시형과 달리 다양한 데이터를 담을 수 있음

키로 구분된 데이터 집합이나 복잡한 개체(entity)를 저장 가능

객체는 중괄호 `{...}`를 이용해 만들 수 있음, 중괄호 안에는 `키(key): 값(value)` 쌍으로 구성된 `프로퍼티(property)`를 여러 개 넣을 수 있는데, `키`엔 문자형, `값`엔 모든 자료형이 허용

빈 객체를 만드는 방법은 두 가지가 있음

```jsx
let obj = new Object(); // '객체 생성자' 문법
let obj = {}; // '객체 리터럴' 문법
```

## 리터럴과 프로퍼티

중괄호 `{...}` 안에는 `키: 값`쌍으로 구성된 프로퍼티가 들어감

```jsx
let 객체명 = {
  키: 값,
  name: 'Jun'
};
```

`콜론(;)`을 기준으로 왼쪽에는 키가, 오른쪽에는 값이 위치

프로퍼티 키는 `프로퍼티 이름` 혹은 `프로퍼티 식별자`라고도 부름

여러 단어를 조합해 프로퍼티 이름을 만드는 경우 프로퍼티 이름을 따옴표로 묶어줘야 함

마지막 프로퍼티 끝은 쉼표로 끝날 수 있음, 이런 쉼표를 `trailing(길게 늘어지는)` 혹은 `hanging(매달리는)` 쉼표라고 부름 → 모든 프로퍼티가 유사한 형태를 보이기 때문에 프로퍼티를 추가, 삭제, 이동하는게 쉬워짐

### 프로퍼티 추가

```jsx
객체명.키 = 값;
```

프로퍼티 값에는 모든 자료형이 들어갈 수 있음

### 프로퍼티 삭제

```jsx
delete 객체명.키;
```

### 상수 객체

`const`로 선언된 객체는 수정될 수 있음

`const`로 선언한 객체는 객체의 값을 고정하지만, 그 내용은 고정하지 않음

### 대괄호 표기법

여러 단어를 조합해 프로퍼티 키를 만든 경우엔, 점 표기법을 사용해 프로퍼티 값을 읽을 수 없음

`.`은 `유효한 변수 식별자`일 경우에만 사용할 수 있음, 유효한 변수 식별자엔 공백이 없어야 하며, 숫자로 시작하지 않고 `$`와 `_`를 제외한 특수 문자가 없어야 함

키가 유효한 변수 식별자가 아닌 경우엔 `대괄호 표기법(square bracket notation)`이라 불리는 방법을 사용할 수 있음

```jsx
객체명[식별자];
```

대괄호 표기법 안에서 문자열을 사용할 땐 문자열을 따옴표로 묶어줘야 함

대괄호 표기법을 사용하면 문자열 뿐만 아니라 변수 등의 모든 표현식의 평가 결과를 프로퍼티 키로 사용 가능

### 계산된 프로퍼티

객체를 만들 때 객체 리터럴 안의 프로퍼티 키가 대괄호로 둘러싸여 있는 경우, 이를 `계산된 프로퍼티(computed property)`라고 부름

```jsx
let fruit = prompt('어떤 과일을 구매하시겠습니까?', 'apple');

let bag = {
  [fruit]: 5, // 변수 fruit에서 프로퍼티 이름을 동적으로 받아 옴
};
// 아래의 코드와 같은 동작
let bag = {};
bag[fruit] = 5;
```

### 단축 프로퍼티

프로퍼티 값을 기존 변수에서 받아와 사용하는 경우 프로퍼티의 이름과 값이 동일할 때 `프로퍼티 값 단축 구문(property value shorthand)`을 사용하면 코드를 짧게 줄일 수 있음

```jsx
function makeUser(name, age) {
  return {
    name, // name: name과 같음
    age, // age: age와 같음
  }
}
```

### 프로퍼티 이름의 제약사항

변수 이름엔 예약어를 사용하면 안되지만 객체 프로퍼티엔 이러한 제약이 없음

객체 프로퍼티 키에 쓸 수 있는 문자열엔 제약이 없지만, `__proto__`는 일반적으로 사용할 수 없음

### `in` 연산자로 프로퍼티 존재 여부 확인하기

```jsx
"key" in object
```

### `for...in` 반복문

`for...in` 반복문을 사용하면 객체의 모든 키를 순회할 수 있음

```jsx
for (key in object) {
  // 각 프로퍼티 키(key)를 이용하여 본문을 실행
}

// 예제
let user = {
  name: "Jun",
  age: 26,
  isAdmin: true,
}

for(let key in user) {
  alert(key); // name, age, isAdmin
  alert(user[key]); // Jun, 26, true
}
```

반복 변수명은 key 말고도 다른 변수명을 자유롭게 사용할 수 있음

## 객체 정렬 방식

객체의 프로퍼티는 `정수 프로퍼티(integer property)`는 자동으로 정렬되고, 그 외의 프로퍼티는 객체에 추가한 순서 그대로 정렬

## 참조에 의한 객체 복사

객체와 원시 타입의 근본적인 차이 중 하나는 객체는 `참조에 의해(by reference)` 저장되고 복사됨

원기값은 `값 그대로` 저장·할당되고 복사되지만 객체는 객체가 저장되어있는 `메모리 주소`인 객체에 대한 `참조 값`이 저장

### 참조에 의한 비교

객체 비교 시 동등 연산자 `==`와 일치 연산자 `===`는 동일하게 동작

```jsx
let a = {};
let b = a;

alert(a == b); // true
alert(a === b); // true

b = {};

alert(a == b); // false;
```

### 객체 복사, 병합과 Object.assign

객체를 복제하기 위해서는 새로운 객체를 만든 다음 기존 객체의 프로퍼티들을 순회해 원시 수준까지 프로퍼티를 복사

```jsx
let user = {
  name: 'Jun',
  age: 26,
}

let clone = {}; // 새로운 빈 객체 생성

// 빈 객체에 user 프로퍼티 전부를 복사
for (let key in user) {
  clone[key] = user[key];
}
```

`Object.assign`를 사용하는 방법도 있음

```jsx
Object.assign(dest, [src1, src2, src3 ...]);
```

- 첫 번째 인수 `dest`는 목표로 하는 객체
- 이어지는 인수 `src1, ..., srcN`는 복사하고자 하는 객체
- 객체 `src1, ..., srcN`의 프로퍼티를 `desc`에 복사, `desc`를 제외한 인수(객체)의 프로퍼티 전부가 첫 번째 인수(객체)로 복사
- `desc` 반환

목표 객체에 동일한 이름을 가진 프로퍼티가 있는 경우에는 기존 값이 덮어씌워 짐

### 중첩 객체 복사

프로퍼티는 다른 객체에 대한 참조 값일 수도 있음

```jsx
let user = {
  name: 'Jun',
  age: 26,
  sizes: {
    height: 176,
    width: 80,
  }
};
```

`user.sizes`의 경우 객체이기 때문에 `Object.assign`을 사용해 복사하여도 같은 `sizes`를 공유

이 문제를 해결하기 위해서는 `user[key]`의 값을 검사하면서, 그 값이 객체인 경우 객체의 구조도 복사해주는 반복문을 사용해야 함

이런 방식을 `깊은 복사(deep cloning)`

자바스크립트 라이브러리 [lodash](https://lodash.com/)의 메서드인 `_.cloneDeep(obj)`를 사용하면 깊은 복사를 처리할 수 있음

## 메서드

객체 프로퍼티에 할당된 함수를 `메서드(method)`라고 함

함수 표현식으로 함수를 만들고, 객체 프로퍼티에 함수를 할당해 주면 객체에 함수를 할당할 수 있음

### 메서드 단축 구문

```jsx
let user = {
  sayHi: function() {
    alert('Hi!');
  }
};

// 단축 구문
let user = {
  sayHi() {
    alert('Hi!');
  }
};
```

### this

매서드 내부에서 `this` 키워드를 사용하면 객체에 접근할 수 있음

`this` 키워드는 메서드를 호출할 때 사용된 객체를 나타냄

```jsx
let user = {
  name: 'Jun',
  age: 26,
  
  sayHi() {
    alert(this.name);
  }
};

user.sayHi(); // Jun
```

화살표 함수는 일반 함수와 달리 고유한 `this`를 가지지 않음

화살표 함수에서 `this`를 참조하면 외부 함수에서 `this` 값을 가져옴

## 생성자 함수

`생성자 함수(constructor function)`와 일반 함수에 기술적인 차이는 없지만 생성자 함수는 다음을 따름

1. 함수 이름의 첫 글자는 대문자로 시작
2. 반드시 `new` 연산자를 붙여 실행

```jsx
function User(name) {
  this.name = name;
  this.isAdmin = false;
}

let user = new User('Jun');
```

`new` 연산자를 써서 함수를 실행하면 아래와 같은 알고리즘이 동작

1. 빈 객체를 만들어 `this`에 할당
2. 함수 본문을 실행, `this`에 새로운 프로퍼티를 추가해 `this`를 수정
3. `this`를 반환

## new.target과 생성자 함수

`new.target` 프로퍼티를 사용하면 함수가 `new`와 함께 호출되었는지 아닌지를 알 수 있음

일반적인 방법으로 함수를 호출했다면 `undefined`를 반환, `new`와 함께 호출한 경우엔 `new.target`은 함수 자체를 반환

이를 활용해 일반적인 방법으로 함수를 호출해도 `new`를 붙여 호출한 것과 같이 동작하도록 만들 수 있음

```jsx
function User(name) {
  if (!new.target) {
    return new User(name);
  }

  this.name = name;
}
```

## 생성자 내 메서드

```jsx
function User(name) {
  this.name = name;
  
  this.sayHi = function() {
    alert(`My name is ${this.name}, Hi!`);
  };
}
```

# 옵셔널 체이닝 `?.`

`옵셔널 체이닝(optional chaining)` `?.`을 사용하면 프로퍼티가 없는 중첩 객체를 에러 없이 안전하게 접근할 수 있음

`?.`은 `?.` 앞의 평가 대상이 `undefined`나 `null`이면 평가를 멈추고 `undefined`를 반환

`.?`는 `()`나 `[]`와도 같이 사용할 수 있음, 또한 `delete`와 조합해 사용할 수도 있음

```jsx
user.admin?.(); // user 객체에 admin() 함수가 존재하면 함수 실행

delete user.?name; // user가 존재하면 user.name을 삭제
```

# 심볼형

자바스크립트는 객체 프로퍼티 키로 오직 `문자형`과 `심볼형`만 허용

`심볼(symbol)`은 `유일한 식별자(unique identifier)`를 만들고 싶을 때 사용

`Symbol()`을 사용하여 심볼값을 만들 수 있음

```jsx
let id = Symbol();
```

심볼을 만들 때 심볼 이름이라 불리는 설명을 붙일 수 있음

```jsx
let id = Symbol('id');
```

심볼은 유일성이 보장되는 자료형이기 때문에, 설명이 동일한 심볼을 여러 개 만들어도 각 심볼값은 다름, 심볼에 붙이는 설명(심볼 이름)은 어떤 것에도 영향을 주지 않는 이름표 역할

```jsx
let id1 = Symbol('id');
let id2 = Symbol('id');

alert(id1 == id2); // false
```

## '숨김' 프로퍼티

심볼을 이용하면 `숨김(hidden) 프로퍼티`를 만들 수 있음

`숨김 프로퍼티`는 외부 코드에서 접근이 불가능하고 값도 덮어쓸 수 없는 프로퍼티

```jsx
let user = { // 서드파티 코드에서 가져온 객체
  name: 'Jun',
};

let id = Symbol('id');

user[id] = 1;

alert(user[id]); // 1

// ... 제3 스크립트
let id = Symbol('id');

user[id] = '제3 스크립트 id 값';
```

위 상황에서 심볼은 유일성이 보장되므로 현재 작업 중인 스크립트의 식별자와 제3의 스크립트에서 만든 식별자는 충돌하지 않음

## Symbols in a literal

객체 리터럴 `{...}`을 사용해 객체를 만든 경우, 대괄호를 사용해 심볼형 키를 만들어야 함

```jsx
let id = Symbol('id');

let user = {
  name: 'jun',
  [id]: 1,
};
```

## 심볼은 `for...in`에서 배제

키가 심볼인 프로퍼티는 `for...in`에서 배제

`Object.keys(객체)`에서도 키가 심볼인 프로퍼티는 배제됨

하지만 `Object.assign`은 키가 심볼인 프로퍼티를 배제하지 않고 객체 내 모든 프로퍼티를 복사

## 전역 심볼

`전역 심볼 레지스트리(global symbol registry)` 안에 심볼을 만들고 해당 심볼에 접근하면, 이름이 같은 경우 항상 동일한 심볼을 반환

레지스트리 안에 있는 심볼을 읽거나, 새로운 심볼을 생성하려면 `Symbol.for(key)`를 사용

`Symbol.for()` 메서드를 호출하면 이름이 `key`인 심볼을 반환하고 조건에 맞는 심볼이 레지스트리 안에 없으면 새로운 심볼 `Symbol(key)`를 만들고 레지스트리 안에 저장

### Symbol.keyFor

`Symbol.keyFor()` 메서드는 전역 심볼을 찾을 때 사용되는 `Symbol.for()`에 반대되는 메서드

`Symbol.keyFor()`를 사용하면 심볼을 이용해 이름을 얻음

```jsx
let sym = Symbol.for('name');

alert(Symbol.keyFor(sym)); // name
```

`Symbol.keyFor()`는 전역 심볼 레지스트리에서 해당 심볼을 얻어내므로 전역 심볼이 아닌 인자가 넘어오면 `undefined`를 반환

전역 심볼이 아닌 모든 심볼은 `description` 프로퍼티를 사용하여 이름을 얻음

## 시스템 심볼

`시스템 심볼(system symbol)`은 자바스크립트 내부에서 사용되는 심볼

- Symbol.hasInstance
- Symbol.isConcatSpreadable
- Symbol.iterator
- Symbol.toPrimitive
- 기타 등등

# 객체를 원시형으로 변환하기

## Symbol.toPrimitive

자바스크립트엔 `Symbol.toPrimitive`라는 내장 심볼이 존재

```jsx
obj[Symbol.toPrimitive] = function(hint) {
  // 반드시 원시값을 반환
  // hint는 'string', 'number', 'default' 중 하나
};

// 예제
let user = {
  name: 'Jun',
  money: 100,

  [Symbol.toPrimitive](hint) {
    return hint == 'string' ? `{name: "${this.name}"}` : this.money;
  }
};

alert(user); // hint: string // {name: "Jun"}
alert(+user); // hint: number // 100
alert(user + 500); // hint: default // 600
```

# 원시값의 메서드

1. 원시값은 원시값 그대로 남겨둬 단일 값 형태를 유지
2. 문자열, 숫자, 불린, 심볼의 메서드와 프로퍼티에 접근할 수 있도록 언어 차원에서 허용
3. 이를 가능하게 하기 위해, 원시값이 메서드나 프로퍼티에 접근하려 하면 추가 기능을 제공해주는 특수한 객체, `원시 래퍼 객체(object wrapper)`를 만듦, 이 객체는 작업 후 삭제

```jsx
let str = "Hello, world!';
alert(str.toUpperCase()); // HELLO, WORLD!
```

1. 문자열 `str`은 원시값이므로 원시값의 프로퍼티에 접근하는 특별한 객체 생성
2. 메서드가 실행되고, 새로운 문자열이 반환
3. 특별한 객체는 삭제되고, 원시값 `str`만 남게 됨